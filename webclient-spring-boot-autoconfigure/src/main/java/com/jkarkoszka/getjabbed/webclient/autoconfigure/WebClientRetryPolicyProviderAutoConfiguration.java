package com.jkarkoszka.getjabbed.webclient.autoconfigure;

import com.jkarkoszka.getjabbed.webclient.RetryBackoffPolicyProvider;
import com.jkarkoszka.getjabbed.webclient.RetryFixedDelayPolicyProvider;
import lombok.AllArgsConstructor;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;

/**
 * Autoconfiguration for retry policy providers.
 */
@Configuration
@EnableConfigurationProperties(WebClientProperties.class)
@PropertySource("classpath:webclient.properties")
@AllArgsConstructor
public class WebClientRetryPolicyProviderAutoConfiguration {

  private final WebClientProperties webClientProperties;

  @Bean
  RetryBackoffPolicyProvider retryBackoffPolicyProvider() {
    return new RetryBackoffPolicyProvider(webClientProperties.getRetryBackoffPolicy());
  }

  @Bean
  RetryFixedDelayPolicyProvider retryFixedDelayPolicyProvider() {
    return new RetryFixedDelayPolicyProvider(webClientProperties.getRetryFixedDelayPolicy());
  }
}
