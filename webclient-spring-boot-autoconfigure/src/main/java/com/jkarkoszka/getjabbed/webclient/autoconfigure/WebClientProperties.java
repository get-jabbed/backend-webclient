package com.jkarkoszka.getjabbed.webclient.autoconfigure;

import com.jkarkoszka.getjabbed.webclient.BaseRetryBackoffPolicyProperties;
import com.jkarkoszka.getjabbed.webclient.BaseRetryFixedDelayPolicyProperties;
import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;

/**
 * Properties for the webclient.
 */
@ConfigurationProperties(prefix = "getjabbed.webclient")
@Data
public class WebClientProperties {

  private RetryBackoffPolicyProperties retryBackoffPolicy;
  private RetryFixedDelayPolicyProperties retryFixedDelayPolicy;

  /**
   * Properties for the backoff policy.
   */
  @Data
  public static class RetryBackoffPolicyProperties implements BaseRetryBackoffPolicyProperties {

    private long periodInSeconds;
    private int maxAttempts;
  }

  /**
   * Properties for the fixed delay policy.
   */
  @Data
  public static class RetryFixedDelayPolicyProperties
      implements BaseRetryFixedDelayPolicyProperties {

    private long periodInSeconds;
    private int maxAttempts;
  }
}
