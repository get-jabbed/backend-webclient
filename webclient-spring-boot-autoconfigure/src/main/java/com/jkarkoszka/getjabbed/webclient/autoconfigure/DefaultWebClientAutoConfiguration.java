package com.jkarkoszka.getjabbed.webclient.autoconfigure;

import org.springframework.boot.autoconfigure.AutoConfigureAfter;
import org.springframework.boot.autoconfigure.condition.ConditionalOnMissingBean;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.reactive.function.client.WebClient;

/**
 * Autoconfiguration for web client (default).
 */
@Configuration
@AutoConfigureAfter(WebClientAutoConfiguration.class)
public class DefaultWebClientAutoConfiguration {

  @Bean
  @ConditionalOnMissingBean
  WebClient webClient() {
    return WebClient.builder().build();
  }
}
