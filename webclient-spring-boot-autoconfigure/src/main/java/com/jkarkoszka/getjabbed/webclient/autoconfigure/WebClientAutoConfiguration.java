package com.jkarkoszka.getjabbed.webclient.autoconfigure;

import brave.http.HttpTracing;
import lombok.AllArgsConstructor;
import org.springframework.boot.autoconfigure.AutoConfigureAfter;
import org.springframework.boot.autoconfigure.AutoConfigureBefore;
import org.springframework.boot.autoconfigure.condition.ConditionalOnBean;
import org.springframework.boot.autoconfigure.condition.ConditionalOnClass;
import org.springframework.cloud.sleuth.autoconfig.brave.BraveAutoConfiguration;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.client.reactive.ReactorClientHttpConnector;
import org.springframework.web.reactive.function.client.WebClient;
import org.zalando.logbook.Logbook;
import org.zalando.logbook.netty.LogbookClientHandler;
import reactor.netty.http.brave.ReactorNettyHttpTracing;
import reactor.netty.http.client.HttpClient;

/**
 * Autoconfiguration for web client (with logbook).
 */
@Configuration
@ConditionalOnClass({WebClient.class, BraveAutoConfiguration.class})
@AutoConfigureBefore(WebClientLogbookAutoConfiguration.class)
@AutoConfigureAfter(BraveAutoConfiguration.class)
@AllArgsConstructor
public class WebClientAutoConfiguration {

  @Bean
  @ConditionalOnBean(HttpTracing.class)
  ReactorNettyHttpTracing reactorNettyHttpTracing(HttpTracing httpTracing) {
    return ReactorNettyHttpTracing.create(httpTracing);
  }

  @Bean
  @ConditionalOnBean(ReactorNettyHttpTracing.class)
  WebClient webClient(Logbook logbook, ReactorNettyHttpTracing tracing) {
    return WebClient
        .builder()
        .clientConnector(
            new ReactorClientHttpConnector(
                tracing.decorateHttpClient(
                    HttpClient.create()
                        .doOnConnected(
                            conn -> conn.addHandlerLast(new LogbookClientHandler(logbook))))))
        .build();
  }
}
