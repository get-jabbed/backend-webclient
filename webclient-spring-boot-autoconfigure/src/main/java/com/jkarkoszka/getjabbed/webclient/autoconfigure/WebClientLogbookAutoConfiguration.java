package com.jkarkoszka.getjabbed.webclient.autoconfigure;

import lombok.AllArgsConstructor;
import org.springframework.boot.autoconfigure.AutoConfigureBefore;
import org.springframework.boot.autoconfigure.condition.ConditionalOnClass;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.reactive.function.client.WebClient;
import org.zalando.logbook.DefaultHttpLogFormatter;
import org.zalando.logbook.DefaultSink;
import org.zalando.logbook.autoconfigure.LogbookAutoConfiguration;

/**
 * Autoconfiguration of the logbook sink.
 */
@Configuration
@ConditionalOnClass({WebClient.class})
@AutoConfigureBefore(LogbookAutoConfiguration.class)
@AllArgsConstructor
public class WebClientLogbookAutoConfiguration {

  @Bean
  DefaultSink defaultSink() {
    return new DefaultSink(new DefaultHttpLogFormatter(), new InfoLevelHttpLogWriter());
  }
}
