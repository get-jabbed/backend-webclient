package testproject;

import static org.mockserver.integration.ClientAndServer.startClientAndServer;
import static org.mockserver.matchers.Times.exactly;
import static org.mockserver.model.HttpRequest.request;
import static org.mockserver.model.HttpResponse.response;

import org.mockserver.integration.ClientAndServer;
import org.mockserver.model.Header;
import org.springframework.stereotype.Component;

@Component
public class SampleApiMock {

  private static int PORT = 20010;
  public static String SAMPLE_API_URL = "http://localhost:" + PORT + "/sample-api";

  private ClientAndServer clientAndServer;

  public SampleApiMock() {
    clientAndServer = startClientAndServer(PORT);
  }

  public void reset() {
    clientAndServer.reset();
  }

  public void createSampleMock(String requestBody, String responseBody,
                               int status, int exactlyTimes) {
    clientAndServer
        .when(
            request()
                .withMethod("POST")
                .withBody(requestBody)
                .withPath("/sample-api"),
            exactly(exactlyTimes))
        .respond(
            response()
                .withStatusCode(status)
                .withHeaders(new Header("Content-Type", "application/json; charset=utf-8"),
                    new Header("Keep-Alive", "max, unknown=test, timeout=10"))
                .withBody(responseBody));
  }
}
