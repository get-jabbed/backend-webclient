package testproject;

import static testproject.SampleApiMock.SAMPLE_API_URL;

import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpMethod;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.reactive.function.BodyInserters;
import org.springframework.web.reactive.function.client.WebClient;
import reactor.core.publisher.Mono;

@RestController
@AllArgsConstructor
@Slf4j
public class HelloController {

  @Autowired
  private final WebClient webClient;

  @PostMapping("/hello")
  public Mono<String> hello(@RequestBody String requestBody) {
    log.info("test logging before request");
    return webClient.method(HttpMethod.POST)
        .uri(SAMPLE_API_URL)
        .body(BodyInserters.fromValue(requestBody))
        .retrieve()
        .bodyToMono(String.class)
        .doOnSuccess(t -> log.info("test logging after request"));
  }
}
