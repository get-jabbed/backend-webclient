package com.jkarkoszka.getjabbed.webclient.autoconfigure;

import static org.assertj.core.api.AssertionsForClassTypes.assertThat;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.reactive.function.client.WebClient;
import testproject.SampleApiMock;
import testproject.TestApplication;

@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.DEFINED_PORT, classes = TestApplication.class)
public class WebClientAutoConfigurationTest {

  @Autowired
  WebClient webClient;

  @Autowired
  SampleApiMock sampleApiMock;

  @Autowired
  RestTemplate testRestTemplate;

  @BeforeEach
  void setUp() {
    sampleApiMock.reset();
  }

  @Test
  public void shouldStartApplicationCorrectly() {
    assertThat(webClient).isNotNull();
  }

  @Test
  public void shouldPerformRequestCorrectly() {
    //given
    var requestBody = "{\"name\": \"jakub\"}";
    var expectedResponseBody = "{\"result\": \"ok\"}";
    sampleApiMock.createSampleMock(requestBody, expectedResponseBody, 200, 1);

    //when
    var actualResponseBody =
        testRestTemplate.postForEntity("/hello", requestBody, String.class).getBody();

    //then
    assertThat(actualResponseBody).isEqualTo(expectedResponseBody);
  }
}
