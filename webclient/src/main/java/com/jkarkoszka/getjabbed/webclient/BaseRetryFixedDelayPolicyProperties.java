package com.jkarkoszka.getjabbed.webclient;

/**
 * Base retry fixed delay policy proprties.
 */
public interface BaseRetryFixedDelayPolicyProperties {

  long getPeriodInSeconds();

  int getMaxAttempts();
}
