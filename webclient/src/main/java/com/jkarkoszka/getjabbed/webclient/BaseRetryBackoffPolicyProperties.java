package com.jkarkoszka.getjabbed.webclient;

/**
 * Base class for retry backoff policy properties.
 */
public interface BaseRetryBackoffPolicyProperties {

  long getPeriodInSeconds();

  int getMaxAttempts();
}
