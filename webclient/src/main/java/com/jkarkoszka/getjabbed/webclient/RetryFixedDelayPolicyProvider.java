package com.jkarkoszka.getjabbed.webclient;

import java.time.Duration;
import lombok.AllArgsConstructor;
import reactor.util.retry.Retry;
import reactor.util.retry.RetryBackoffSpec;

/**
 * Class to provide ready retry fixed delay spec object.
 */
@AllArgsConstructor
public class RetryFixedDelayPolicyProvider {

  private final BaseRetryFixedDelayPolicyProperties retryFixedDelayPolicyProperties;

  /**
   * Method to provide fixed delay retry policy.
   *
   * @return retry backoff policy
   */
  public RetryBackoffSpec provide() {
    return Retry
        .fixedDelay(retryFixedDelayPolicyProperties.getMaxAttempts(),
            Duration.ofSeconds(retryFixedDelayPolicyProperties.getPeriodInSeconds()));
  }
}
