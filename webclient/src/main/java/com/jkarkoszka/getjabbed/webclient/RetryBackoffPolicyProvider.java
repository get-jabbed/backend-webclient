package com.jkarkoszka.getjabbed.webclient;

import java.time.Duration;
import lombok.AllArgsConstructor;
import reactor.util.retry.Retry;
import reactor.util.retry.RetryBackoffSpec;

/**
 * Class to provide ready retry backoff spec object.
 */
@AllArgsConstructor
public class RetryBackoffPolicyProvider {

  private final BaseRetryBackoffPolicyProperties backoffPolicyProperties;

  /**
   * Method to provide backoff retry policy.
   *
   * @return retry backoff spec
   */
  public RetryBackoffSpec provide() {
    return Retry
        .backoff(backoffPolicyProperties.getMaxAttempts(),
            Duration.ofSeconds(backoffPolicyProperties.getPeriodInSeconds()));
  }
}
